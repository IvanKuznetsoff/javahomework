/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package main.java.animals;

import java.util.Arrays;

/**
 *
 * @author ikuznetsov
 */
public class Kotik {
    private String name;
    private String voice;
    private int satiety;
    private int weight;
    
    static int count;
    
    final int METHODS = 5;
    
    public Kotik(String name, String voice, int satiety, int weight){
        this.name = name;
        this.voice = voice;
        this.satiety = satiety;
        this.weight = weight;
        count ++;
        
    }
    
     public Kotik(){
        count ++; 
    }
    
    public String getName(){
        return name;
    }
    
    public String getVoice(){
        return voice;
    }
    
    public int getSatiety(){
        return satiety;
    }
    
    public int getWeight(){
        return weight;
    }
    
    public static int getCount(){
        return count;
    }
     
    public void setName(String name){
        this.name = name;
    }
    
    public void setVoice(String voice){
        this.voice = voice;
    }
    
    
    public void setSatiety(int satiety){
        this.satiety = satiety;
    }
    
    public void setWeight(int weight){
        this.weight = weight;
    }
   
    
    boolean play(){
        if (this.satiety > 0){
            System.out.println("kotik is playing");
            this.satiety --;
            return true;
        }
        else {
            System.out.println("kotik want`s kushatt");
            return false;
        }
        
       
    }
    
    boolean sleep(){
         if (this.satiety > 0){
            System.out.println("kotik is sleeping");
            this.satiety --;
            return true;
        }
         else {
            System.out.println("kotik want`s kushatt");
            return false;
        } 
    }
    
    boolean wash(){
          if (this.satiety > 0){
            System.out.println("kotik is washing");
            this.satiety --;
            return true;
        }
          else {
            System.out.println("kotik want`s kushatt");
            return false;
        }
    }
    
    boolean walk(){
        if (this.satiety > 0){
            System.out.println("kotik is walking");
            this.satiety --;
            return true;
        }
        else {
            System.out.println("kotik want`s kushatt");
            return false;
        } 
    }
    
    boolean hunt(){
        if (this.satiety > 0){
            System.out.println("kotik is hunting");
            this.satiety --;
            return true;
        }
        else {
            System.out.println("kotik want`s kushatt");
            return false;
        }
    }
    
    public void eat(int satiety){
        this.satiety += satiety;
    }
    
    void eat (int satiety, String inFoodName){
       this.satiety += satiety;
       String outFoodName = inFoodName;
    }
    
    void eat(){
       eat(25, "kaki");
       
    }
    
    public String[] liveAnotherDay() {
        
        String[] finalReport = new String[24];
        String hourReport;
         
        for (int i = 0; i < 24; i++){
          int number = 1 + (int) (Math.random() * METHODS);
         
         
          switch(number){
              case 1 -> {
                  boolean result = play();
                  if (result == false){
                    eat(2);
                    hourReport = Integer.toString(i) + " - " + "kushal"; 
                    finalReport[i] = hourReport;
                  }
                  else{
                    hourReport = Integer.toString(i) + " - " + "igral"; 
                    finalReport[i] = hourReport;
                  }
              }
                  
              case 2 -> {
                  boolean result = sleep();
                  if (result == false){
                    eat(2);
                    hourReport = Integer.toString(i) + " - " + "kushal"; 
                    finalReport[i] = hourReport;
                  }
                  else{
                    hourReport = Integer.toString(i) + " - " + "spal"; 
                    finalReport[i] = hourReport;
                }
              }
                  
              case 3 -> {
                  boolean result = wash();
                  if (result == false){
                    eat(25);
                    hourReport = Integer.toString(i) + " - " + "kushal"; 
                    finalReport[i] = hourReport;
                  }
                  else{
                    hourReport = Integer.toString(i) + " - " + "mylsya"; 
                    finalReport[i] = hourReport;
                }
              }    
              case 4 -> {
                  boolean result = walk();
                  if (result == false){
                      eat(25);
                      hourReport = Integer.toString(i) + " - " + "kushal"; 
                      finalReport[i] = hourReport;
                  }
                  else{
                    hourReport = Integer.toString(i) + " - " + "gulyal"; 
                    finalReport[i] = hourReport;
                }
              }    
              case 5 -> {
                  boolean result = hunt();
                  if (result == false){
                      eat(1);
                      hourReport = Integer.toString(i) + " - " + "kushal"; 
                      finalReport[i] = hourReport;
                  }
                  else{
                    hourReport = Integer.toString(i) + " - " + "okhotilsya"; 
                    finalReport[i] = hourReport;
                    }
            
                }
        
            }   
    
    
  
        }
        return finalReport;  
    }
}

