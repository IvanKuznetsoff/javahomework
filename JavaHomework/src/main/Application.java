/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package main;
import java.util.Arrays;
import main.java.animals.Kotik;

/**
 *
 * @author ikuznetsov
 */
public class Application {
    public static void main(String[] args){
        Kotik kotik1 = new Kotik("murzick","пше сщььше",0, 100);
        Kotik kotik2 = new Kotik();
       
        
        kotik2.setName("barsick");
        kotik2.setVoice("пше згыр");
        kotik2.setSatiety(0);
        kotik2.setWeight(500);
        
        String[] activityResult = kotik2.liveAnotherDay();
        for(String item : activityResult){
            System.out.println(item);
        }
        
        System.out.println();
        System.out.println("Имя экземпляра кота: " + kotik1.getName()+ '\n' + "Вес экземпляра кота: " + kotik1.getWeight() + " кг" + '\n' );
        System.out.println("Результат сравнения кэтвойсов: " + compareVoice(kotik1, kotik2) + '\n');
        System.out.println("Количество созданных котиков: " + Kotik.getCount());  
        
    }
    
    static boolean compareVoice(Kotik voice1, Kotik voice2){
        String result1 = voice1.getVoice();
        String result2 = voice2.getVoice();
        
        boolean finalResult = voice1.equals(voice2);
        return finalResult;  
    }
}
